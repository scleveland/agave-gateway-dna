'use strict';
var lrSnippet = require('grunt-contrib-livereload/lib/utils').livereloadSnippet;
var mountFolder = function(connect, dir) {
  return connect.static(require('path').resolve(dir));
};

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to match all subfolders:
// 'test/spec/**/*.js'

module.exports = function(grunt) {
  // load all grunt tasks
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  // configurable paths
  var yeomanConfig = {
    app: 'app',
    dist: 'dist',
    debug: 'debug'
  };

  grunt.initConfig({
    yeoman: yeomanConfig,
    watch: {
      compass: {
        files: ['<%= yeoman.app %>/styles/{,*/}*.{scss,sass}'],
        tasks: ['compass']
      },
      livereload: {
        files: [
          '<%= yeoman.app %>/*.html',
          '<%= yeoman.app %>/templates/**/*.html',
          '{.tmp,<%= yeoman.app %>}/styles/{,*/}*.css',
          '{.tmp,<%= yeoman.app %>}/scripts/{,*/}*.js',
          '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp}'
        ],
        tasks: ['livereload']
      }
    },
    connect: {
      options: {
        port: 9000,
        // change this to '0.0.0.0' to access the server from outside
        //hostname: 'localhost'
        hostname: '0.0.0.0'
      },
      livereload: {
        options: {
          middleware: function(connect) {
            return [
              lrSnippet,
              mountFolder(connect, '.tmp'),
              mountFolder(connect, 'app')
            ];
          }
        }
      },
      test: {
        options: {
          middleware: function(connect) {
            return [
              mountFolder(connect, '.tmp'),
              mountFolder(connect, 'test')
            ];
          }
        }
      },
      debug: {
        options: {
          middleware: function(connect) {
            return [
              mountFolder(connect, 'debug')
            ];
          }
        }
      },
      dist: {
        options: {
          middleware: function(connect) {
            return [
              mountFolder(connect, 'dist')
            ];
          }
        }
      }
    },
    // open: {
    //   server: {
    //     path: 'http://localhost:<%= connect.options.port %>'
    //   }
    // },
    clean: {
      dist: ['.tmp', '<%= yeoman.dist %>/*'],
      debug: ['.tmp', '<%= yeoman.debug %>/*'],
      server: '.tmp'
    },
    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      all: [
        'Gruntfile.js',
        '<%= yeoman.app %>/scripts/{,*/}*.js',
        '!<%= yeoman.app %>/scripts/vendor/*',
        'test/spec/{,*/}*.js'
      ]
    },
    buster: {
      test: {
        config: 'test/buster.js'
      },
      server: {
        port: 1111
      }
    },
    compass: {
      options: {
        sassDir: '<%= yeoman.app %>/styles',
        cssDir: '.tmp/styles',
        imagesDir: '<%= yeoman.app %>/images',
        javascriptsDir: '<%= yeoman.app %>/scripts',
        fontsDir: '<%= yeoman.app %>/styles/fonts',
        importPath: 'app/components',
        relativeAssets: true
      },
      debug: {
        options: {
          outputStyle: 'expanded'
        }
      },
      dist: {},
      server: {
        options: {
          debugInfo: true
        }
      }
    },
    handlebars: {
      compile: {
        options: {
          namespace: 'JST',
          processName: function(filename) {
            return filename.substr(4); // remove 'app/' from filename
          }
        },
        files: {
          '<%= yeoman.dist %>/scripts/templates.js': ['<%= yeoman.app %>/templates/**/*.html']
        }
      }
    },
    // jst: {
    //   compile: {
    //     options: {
    //       processName: function(filename) {
    //         return filename.substr(4);
    //       }
    //     },
    //     files: {
    //       '<%= yeoman.dist %>/scripts/templates.js': ['<%= yeoman.app %>/templates/**/*.html']
    //     }
    //   }
    // },
    concat: {
      dist: {
        src: [
          '<%= yeoman.dist %>/scripts/main.js',
          '<%= yeoman.dist %>/scripts/templates.js'
        ],
        dest: '<%= yeoman.dist %>/scripts/main.js',
        separator: ';'
      }
    },
    requirejs: {
      dist: {
        // Options: https://github.com/jrburke/r.js/blob/master/build/example.build.js
        options: {
          // `name` and `out` is set by grunt-usemin
          baseUrl: '.tmp/scripts',
          optimize: 'none',
          // TODO: Figure out how to make sourcemaps work with grunt-usemin
          // https://github.com/yeoman/grunt-usemin/issues/30
          //generateSourceMaps: true,
          // required to support SourceMaps
          // http://requirejs.org/docs/errors.html#sourcemapcomments
          preserveLicenseComments: false,
          useStrict: true,
          wrap: true,
          //uglify2: {} // https://github.com/mishoo/UglifyJS2
        }
      }
    },
    useminPrepare: {
      html: '<%= yeoman.app %>/index.html',
      options: {
        dest: '<%= yeoman.dist %>'
      }
    },
    usemin: {
      html: ['<%= yeoman.dist %>/{,*/}*.html'],
      css: ['<%= yeoman.dist %>/styles/{,*/}*.css'],
      options: {
        dirs: ['<%= yeoman.dist %>']
      }
    },
    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/images',
          src: '{,*/}*.{png,jpg,jpeg}',
          dest: '<%= yeoman.dist %>/images'
        }]
      }
    },
    svgmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/images',
          src: '{,*/}*.svg',
          dest: '<%= yeoman.dist %>/images'
        }]
      }
    },
    cssmin: {
      dist: {
        files: {
          '<%= yeoman.dist %>/styles/main.css': [
            '.tmp/styles/{,*/}*.css',
            '<%= yeoman.app %>/styles/{,*/}*.css'
          ]
        }
      }
    },
    htmlmin: {
      dist: {
        options: {
          /*removeCommentsFromCDATA: true,
          // https://github.com/yeoman/grunt-usemin/issues/44
          //collapseWhitespace: true,
          collapseBooleanAttributes: true,
          removeAttributeQuotes: true,
          removeRedundantAttributes: true,
          useShortDoctype: true,
          removeEmptyAttributes: true,
          removeOptionalTags: true*/
        },
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>',
          src: '*.html',
          dest: '<%= yeoman.dist %>'
        }]
      }
    },
    // Put files not handled in other tasks here
    copy: {
      prepareRequirejs: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= yeoman.app %>',
          dest: '.tmp',
          src: [
            'components/{,**/}*.js',
            'scripts/{,**/}*.js'
          ]
        },
        {
          expand: false,
          dot: true,
          cwd: '',
          nonull:true,
          dest: '.tmp/components/jquery/jquery.js',
          src: 'app/components/jquery/dist/jquery.js'
        }]
      },
      prepjquery: {
        files: [{
          expand: false,
          dot: true,
          cwd: '',
          nonull:true,
          dest: '<%= yeoman.app %>/components/jquery/jquery.js',
          src: 'app/components/jquery/dist/jquery.js'
        }]
      },
      debug: {
        files: [
          {
            expand: true,
            dot: true,
            cwd: '<%= yeoman.app %>',
            dest: '<%= yeoman.debug %>',
            src: [
              '*.{ico,txt,html}',
              '.htaccess',
              'images/{,*/}*',
              'styles/fonts/*',
              'components/{,**/}*.js',
              'scripts/{,**/}*.js',
              'templates/{,**/}*.html'
            ]
          },
          {
            expand: true,
            dot: true,
            cwd: '.tmp',
            dest: '<%= yeoman.debug %>',
            src: [
              'styles/*.css',
            ]
          }
        ]
      },
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= yeoman.app %>',
          dest: '<%= yeoman.dist %>',
          src: [
            '*.{ico,txt}',
            '.htaccess',
            'images/{,*/}*.{webp,gif}',
            'styles/fonts/*'
          ]
        }]
      }
    },
    bower: {
      options: {
        exclude: ['modernizr']
      },
      all: {
        rjsConfig: '<%= yeoman.app %>/scripts/main.js'
      }
    }
  });

  grunt.renameTask('regarde', 'watch');

  grunt.registerTask('server', function(target) {
    if (target === 'dist') {
      return grunt.task.run(['build', 'open', 'connect:dist:keepalive']);
    }

    grunt.task.run([
      'clean:server',
      'copy:prepjquery',
      'compass:server',
      'livereload-start',
      'connect:livereload',
      // 'open',
      'watch'
    ]);
  });

  grunt.registerTask('test', [
    'clean:server',
    'compass',
    'connect:test'
  ]);

  grunt.registerTask('debug', [
    'jshint',
    'clean:debug',
    'compass:debug',
    'copy:debug'
  ]);

  grunt.registerTask('build', [
    'clean:dist',
    'compass:dist',
    'handlebars',
    'useminPrepare',
    'copy:prepareRequirejs',
    'requirejs',
    'imagemin',
    'svgmin',
    'htmlmin',
    'concat',
    'cssmin',
    'uglify',
    'copy:dist',
    'usemin'
  ]);

  grunt.registerTask('default', [
    'jshint',
    'test',
    'build'
  ]);
};
