/*global define, Backbone, $, _*/
'use strict';
define(['app'], function(App) {
  var AgaveProfile = {};

  AgaveProfile.MyProfile = Backbone.View.extend({
    template: 'profiles/view',
    initialize: function() {
      if (App.Agave.token().isValid()) {
        this.model = new Backbone.Agave.Profile.Profile({username: App.Agave.token().get('username')});
        this.listenTo(this.model, 'change', this.render);
        this.model.fetch();
      }
    },
    beforeRender: function() {
      if (! App.Agave.token().isValid()) {
        this.insertView('.my-profile', new App.Views.Util.Alert({
          model: new App.Models.MessageModel({
            body: 'Log in to view your profile.'
          }),
          type: 'error'
        }));
      } else {
        this.insertView('.actions', new AgaveProfile.Actions());
        if (this.model.get('username')) {
          this.insertView('.my-profile', new AgaveProfile.ProfileView({model: this.model}));
        } else {
          this.insertView('.my-profile', new App.Views.Util.Loading());
        }
      }
    }
  });

  AgaveProfile.Actions = Backbone.View.extend({
    template: 'profiles/actions'
  });

  AgaveProfile.ProfileView = Backbone.View.extend({
    template: 'profiles/profile',
    serialize: function() {
      return this.model.toJSON();
    }
  });

  AgaveProfile.Search = Backbone.View.extend({
    template: 'profiles/search',
    initialize: function() {
      this.listenTo(this.collection, 'reset', this.render);
    },
    beforeRender: function() {
      if (! App.Agave.token().isValid()) {
        this.setView('.search', new App.Views.Util.Alert({
          model: new App.Models.MessageModel({
            body: 'You must be logged in to search users.'
          }),
          type: 'error'
        }));
      } else if (this.collection.length > 0) {
        this.collection.each(function(profile) {
          this.insertView('.results', new AgaveProfile.ProfileView({
            className: 'search-result',
            model: profile
          }));
        }, this);
      } else if (this.__manager__.hasRendered) {
        this.insertView('.search', new App.Views.Util.Alert({
          model: new App.Models.MessageModel({body: 'No results for search.'}),
          type: 'info'
        }));
      }
    },
    serialize: function() {
      return {
        keyword: this.collection.keyword,
        usernameChecked: this.collection.searchOption === 'username',
        nameChecked: this.collection.searchOption === 'name',
        emailChecked: this.collection.searchOption === 'email',
        results: this.collection.length > 0
      };
    },
    events: {
      'click .btn-search': 'doSearch'
    },
    doSearch: function(e) {
      e.preventDefault();
      var view = this;

      var formValues = this.$el.find('form[name=profile-search-form]').serializeArray();

      _.each(formValues, function(val) {
        this.collection[val.name] = val.value;
      }, this);

      this.collection.fetch({
        reset: true,
        beforeSend: function() {
          var loading = new App.Views.Util.Loading();
          view.insertView('.search', loading);
          loading.render();
        },
        error: function(collection, response) {
          var resp = $.parseJSON(response.responseText);
          window.alert(resp.message);
        }
      });

      return false;
    }
  });

  App.Views.AgaveProfile = AgaveProfile;
  return AgaveProfile;
});
