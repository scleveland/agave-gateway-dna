/*global define, Backbone, _*/
'use strict';
define(['app'], function(App) {
  var AgavePostIt = {};

  AgavePostIt.List = Backbone.View.extend({
    tagName: 'ul',
    initialize: function() {
      this.collection = new Backbone.Agave.PostIts.ActivePostIts();
      this.listenTo(this.collection, 'reset', this.render);
      this.collection.fetch({reset:true});
    },
    beforeRender: function() {
      if (this.collection.size() > 0) {
        this.collection.each(function(postit) {
          this.insertView(new AgavePostIt.ListItem({model: postit}));
        }, this);
      } else if (this.__manager__.hasRendered) {
        this.insertView(new App.Views.Util.Alert({
          model: new App.Models.MessageModel({'body':'You currently don\'t have any PostIt URLs.'}),
          type:'info'
        }));
      }
    }
  });

  AgavePostIt.ListItem = Backbone.View.extend({
    tagName: 'li',
    template: 'postits/postit',
    serialize: function() {
      console.log(this.model.toJSON());
      return this.model.toJSON();
    }
  });

  AgavePostIt.CreateForm = Backbone.View.extend({
    tagName:'form',
    template: 'postits/form',
    initialize: function() {
      this.model = new Backbone.Agave.PostIts.PostIt();
    },
    beforeRender: function() {
      var FormViews = App.Views.FormViews;

      this.insertViews({'.form-fields': [
        new FormViews.Text({
          model: new App.Models.FormModel({
            label: 'Target URL',
            help: 'This is the URL that will be invoked by this PostIt',
            name: 'url',
            id: 'url',
            defaultValue: this.model.get('url'),
            required: true
          })
        }),
        new FormViews.Select({
          model: new App.Models.FormModel({
            label: 'Target Method',
            help: 'This is the HTTP Method with which this URL will be invoked',
            name: 'method',
            id: 'method',
            defaultValue: this.model.get('method'),
            options: ['GET','POST','PUT','DELETE'],
            required: true
          })
        }),
        new FormViews.Text({
          model: new App.Models.FormModel({
            label: 'Number of uses',
            help: 'How many times can this PostIt be used?  For unlimited uses enter &quot;-1.&quot;',
            name: 'maxUses',
            id: 'maxUses',
            defaultValue: this.model.get('maxUses') || 1
          })
        }),
        new FormViews.Text({
          model: new App.Models.FormModel({
            label: 'Valid for (seconds)',
            help: 'How long should this PostIt be valid?<br>30 days = 2592000<br>1 day = 604800<br>1 hour = 3600',
            name: 'lifetime',
            id: 'lifetime',
            defaultValue: this.model.get('lifetime') || 2592000
          })
        }),
        new FormViews.Checkbox({
          model: new App.Models.FormModel({
            label: 'Unauthenticated URL',
            help: 'If this url does not need authentication to be invoked, check this box',
            name: 'noauth',
            id: 'noauth',
            defaultValue: this.model.get('noauth')
          })
        }),
      ]});

      if (! App.Agave.token().isValid()) {
        this.insertView('.form-fields',
          new FormViews.Text({
            model: new App.Models.FormModel({
              label: 'Your iPlant Username',
              help: 'Provide your iPlant Username for pre-authenticating your PostIt.',
              name: 'username',
              id: 'username',
              required: true
            })
          })
        );
      }

      this.insertViews({'.form-actions': [
        new FormViews.Button({
          'name': 'postit-create-submit',
          'id': 'postit-create-submit',
          'type': 'submit',
          'class': 'btn btn-success',
          'value': 'Create Post It'
        }),
        new FormViews.Button({
          'name': 'postit-create-cancel',
          'id': 'postit-create-cancel',
          'value': 'Cancel'
        })
      ]});
    },
    events: {
      'click #postit-create-submit': 'submitForm',
      'click #postit-create-cancel': 'cancelForm'
    },
    submitForm: function(e) {
      e.preventDefault();
      var formValues = this.$el.serializeArray();
      var post = {};
      
      _.each(formValues, function(value) {
        post[value.name] = value.value;
      });
      this.model.save({}, {
        emulateJSON: true,
        data: post,
        success: function() {
          Backbone.history.navigate('#postits', {'trigger':true});
        }
      });
      return false;
    },
    cancelForm: function(e) {
      e.preventDefault();
      var fragment;
      if (App.Agave.token().isValid()) {
        fragment = '#postits';
      } else {
        fragment = '#';
      }
      Backbone.history.navigate(fragment, {'trigger':true});
      return false;
    }
  });

  App.Views.AgavePostIt = AgavePostIt;
  return AgavePostIt;
});