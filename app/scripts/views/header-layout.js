/*global define, Backbone*/
'use strict';
define(['app'], function(App){
  var HeaderLayoutView = Backbone.View.extend();

  App.Views.HeaderLayoutView = HeaderLayoutView;
  return HeaderLayoutView;
});
