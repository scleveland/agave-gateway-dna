/*global define, Backbone, _*/
'use strict';
define(['app'], function(App) {
  var AgaveMetadata = {};

  AgaveMetadata.MetadataList = Backbone.View.extend({
    template: 'metadata/list',
    tagName: 'div',
    initialize: function() {
      this.collection = new Backbone.Agave.Metadata.MetadataList();
      this.listenTo(this.collection, 'reset', this.render);
      this.collection.fetch({reset:true});
    },
    beforeRender: function() {
      if (this.collection.size() > 0) {
        this.collection.each(function(metadata) {
          this.insertView(new AgaveMetadata.ListItem({
            className: 'search-result',
            model: metadata}));
        }, this);
      } else if (this.__manager__.hasRendered) {
        this.insertView(new App.Views.Util.Alert({
          model: new App.Models.MessageModel({'body':'You currently don\'t have any Metadata URLs.'}),
          type:'info'
        }));
      }
    }
  });

  AgaveMetadata.ListItem = Backbone.View.extend({
    template: 'metadata/listitem',
    serialize: function() {
      console.log(this.model.toJSON());
      return this.model.toJSON();
    }
  });

  AgaveMetadata.MetadataView = Backbone.View.extend({
    template: 'metadata/view',
    initialize: function() {
      this.model.on('change', this.render, this);
      if (! this.model.collection) {
        this.model.fetch();
      }
    },
    serialize: function() {
      var json = this.model.toJSON();
      if (this.model.collection) {
        json.hasCollection = true;
      }
      return json;
    },
    events: {
      // 'click .back-to-list': 'backToCollection',
      'click .btn-show-permissions': 'showPermissions'
    },
    // backToCollection: function(e) {
    //   e.preventDefault();
    //   this.remove();
    //   return false;
    // },
    showPermissions: function(e) {
      e.preventDefault();
      if (App.Agave.token().isValid()) {
        new AgaveMetadata.MetadataPermissions({
          collection: new Backbone.Agave.Metadata.MetadataPermissionList({ id: this.model.id }),
          el: this.$el.find('.app-form')
        }).render();
        $(e.target).hide();
        this.$el.find('.btn-show-form').show();
      } else {
        alert('You must be logged in to manage metadata permissions.');
      }
    }
  });

  AgaveMetadata.MetadataPermissions = Backbone.View.extend({
    template: 'metadata/permissions',
    initialize: function() {
      this.collection.on('reset', this.render, this);
      this.collection.fetch({reset:true});
    },
    serialize: function() {
      return {permissions: this.collection.toJSON(), metaId: this.collection.metaId};
    },
    events: {
      'click .edit-app-pem': 'editPermission',
      'click .add-app-pem': 'addPermission'
    },
    editPermission: function(e) {
      e.preventDefault();
      var models = this.collection.where({id: e.target.dataset.id});
      if (models) {
        models[0].metaId = this.collection.metaId;
        //var metaPemModel = new Backbone.Agave.Apps.AppPermissionForm({model: models[0]});
        var view = new AgaveMetadata.MetadataPermissionForm({model: models[0]});
        App.Layouts.main.setView('.content', view);
        view.render();
        App.router.navigate('#apps/' + models[0].id + '/pems/edit/' + models[0].get('username'));
        $('html,body').animate({scrollTop:view.$el.position().top - 100});
      }
      return false;
    },
    addPermission: function(e) {
      e.preventDefault();
      var metaModel = new Backbone.Agave.Metadata.MetadataPermission({metaId: this.collection.metaId});
      var view = new AgaveMetadata.MetadataPermissionForm({model: metaModel});
      App.Layouts.main.setView('.content', view);
      view.render();
      App.router.navigate('#meta/data/' + this.collection.metaId + '/pems/edit');
      $('html,body').animate({scrollTop:view.$el.position().top - 100});

      return false;
    }
  });

  AgaveMetadata.MetadataPermissionForm = Backbone.View.extend({
    tagName:'form',
    template: 'metadata/permisison-form',
    initialize: function() {
       this.model.on('change', this.render, this);
       this.model.fetch();
    },
    beforeRender: function() {
      var FormViews = App.Views.FormViews;
      //this.model.fetch();
      var permissionName = this.model.getPermissionName();
      console.log(this.model.toJSON());
      this.insertViews({'.form-fields': [
        new FormViews.Text({
          model: new App.Models.FormModel({
            label: 'Username',
            help: 'This is the user for whom the permission applies',
            name: 'username',
            id: 'username',
            defaultValue: this.model.get('username'),
            required: true
          })
        }),
        new FormViews.Hidden({
          model: new App.Models.FormModel({
            name: 'metaId',
            id: 'metaId',
          defaultValue: this.model.get('metaId')
          })
        }),
        new FormViews.Select({
          model: new App.Models.FormModel({
            label: 'Permission',
            help: 'This is the permission the specified user will have. Owners cannot change their permissions.',
            name: 'permission',
            id: 'permission',
            defaultValue: permissionName,
            options: ['READ','WRITE','EXECUTE','READ_WRITE','READ_EXECUTE','WRITE_EXECUTE', 'ALL', 'NONE'],
            required: true
          })
        }),
      ]});

      this.insertViews({'.form-actions': [
        new FormViews.Button({
          'name': 'pem-create-submit',
          'id': 'pem-create-submit',
          'type': 'submit',
          'class': 'btn btn-success',
          'value': 'Update Permission'
        }),
        new FormViews.Button({
          'name': 'pem-create-cancel',
          'id': 'pem-create-cancel',
          'value': 'Cancel'
        })
      ]});
    },
    events: {
      'click #pem-create-submit': 'submitForm',
      'click #pem-create-cancel': 'cancelForm'
    },
    submitForm: function(e) {
      e.preventDefault();
      var formValues = this.$el.serializeArray();
      var post = {};

      _.each(formValues, function(value) {
        post[value.name] = value.value;
      });
      var metaPem = new Backbone.Agave.Metadata.MetadataPermission({ metaId: post['metaId'], username: post['username'] });
      metaPem.save({}, {
        emulateJSON: true,
        data: post,
        type: 'POST',
        success: function() {
          Backbone.history.navigate('#meta/data/' + post.metaId, {'trigger':true});
        }
      });
      return false;
    },
    cancelForm: function(e) {
      e.preventDefault();
      var fragment;
      if (App.Agave.token().isValid()) {
        fragment = '#meta/data/' + this.model.get('metaId');
      } else {
        fragment = '#';
      }
      Backbone.history.navigate(fragment, {'trigger':true});
      return false;
    }
  });

  AgaveMetadata.CreateForm = Backbone.View.extend({
    tagName:'form',
    template: 'metadata/form',
    initialize: function() {
      this.model = new Backbone.Agave.Metadata.Metadata();
    },
    beforeRender: function() {
      var FormViews = App.Views.FormViews;

      this.insertViews({'.form-fields': [
        new FormViews.Text({
          model: new App.Models.FormModel({
            label: 'Item name',
            help: 'This is name of this metadata item',
            name: 'name',
            id: 'name',
            defaultValue: this.model.get('name'),
            required: true
          })
        }),
        new FormViews.TextArea({
          model: new App.Models.FormModel({
            label: 'Item value',
            help: 'The metadata value to persist.',
            name: 'value',
            id: 'value',
            defaultValue: this.model.get('value'),
            required: true
          })
        }),
        new FormViews.Text({
          model: new App.Models.FormModel({
            label: 'Schema UUID',
            help: 'UUID of the schema that should be used to validate this metadata',
            name: 'schemaId',
            id: 'schemaId',
            defaultValue: this.model.get('schemaId')
          })
        }),
        new FormViews.Text({
          model: new App.Models.FormModel({
            label: 'Associated UUID',
            help: 'This is the uuid of another Agave resource to which this metadata is attached',
            name: 'associatedIds[]',
            id: 'associatedIds',
            defaultValue: this.model.get('associatedIds')
          })
        })
      ]});

      this.insertViews({'.form-actions': [
        new FormViews.Button({
          'name': 'metadata-create-submit',
          'id': 'metadata-create-submit',
          'type': 'submit',
          'class': 'btn btn-success',
          'value': 'Save'
        }),
        new FormViews.Button({
          'name': 'metadata-create-cancel',
          'id': 'metadata-create-cancel',
          'value': 'Cancel'
        })
      ]});
    },
    events: {
      'click #metadata-create-submit': 'submitForm',
      'click #metadata-create-cancel': 'cancelForm'
    },
    submitForm: function(e) {
      e.preventDefault();
      var formValues = this.$el.serializeArray();
      var post = {};
      var password;

      _.each(formValues, function(value) {
          post[value.name] = value.value;
      });
      this.model.save({}, {
        emulateJSON: true,
        data: post,
        password: password,
        success: function() {
          Backbone.history.navigate('#meta/data', {'trigger':true});
        }
      });
      return false;
    },
    cancelForm: function(e) {
      e.preventDefault();
      var fragment;
      if (App.Agave.token().isValid()) {
        fragment = '#meta/data';
      } else {
        fragment = '#';
      }
      Backbone.history.navigate(fragment, {'trigger':true});
      return false;
    }
  });

  App.Views.AgaveMetadata = AgaveMetadata;
  return AgaveMetadata;
});
