/*global define, Backbone, _, $, moment*/
'use strict';
define(['app'], function(App){
  var AgaveIO = {}, UtilViews = App.Views.Util;

  AgaveIO.History = Backbone.View.extend({
    tagName: 'ul',
    template: 'files/history',
    initialize: function() {
      this.collection = new Backbone.Agave.IO.HistoryListing({system: this.collection.system, path: this.collection.path});
      this.listenTo(this.collection, 'reset', this.render);
      this.collection.fetch({reset:true});
    },
    beforeRender: function() {
      if (this.collection.size() > 0) {
        this.collection.each(function(historyItem) {
          this.insertView(new AgaveIO.HistoryItem({model: historyItem}));
        }, this);
      } else if (this.__manager__.hasRendered) {
        this.insertView(new App.Views.Util.Alert({
          model: new App.Models.MessageModel({'body':'You currently don\'t have any PostIt URLs.'}),
          type:'info'
        }));
      }
    },
    serialize: function() {
    	return { path: this.collection.path, system: this.collection.system };
    },
  });

  AgaveIO.HistoryItem = Backbone.View.extend({
    tagName: 'li',
    template: 'files/historyItem',
    serialize: function() {
      console.log(this.model.toJSON());
      return this.model.toJSON();
    }
  });

  // AgaveIO.History = Backbone.View.extend({
//   	tagName: 'ul',
//   	template: 'files/history',
//     initialize: function() {
//       var system = this.collection.system;
//       var path = this.collection.path;
//
//       if (App.Agave.token().isValid()) {
//
//         this.collection.fetch({
//         	reset:true
//         });
//         this.setView('.io-history', new UtilViews.Alert({
//           model: new App.Models.MessageModel({body:'Loading your file/folder history...'}),
//           type: 'info'
//         }));
//         this.collection.on('reset', function() {
// 	        this.setView('.io-history', new AgaveIO.HistoryList({ collection: this.collection, path: path, system: system}));
// 		    this.render();
// 		}, this);
//
//       } else {
//         this.setView('.io-history', new UtilViews.Alert({
//           model: new App.Models.MessageModel({body:'You must be logged in to view your data history.'}),
//           type: 'error'
//         }));
//       }
//     },
//    //  beforeRender: function() {
// //     	 if (this.collection.size() == 0) {
// //    	 	   this.setView('.io-history', new UtilViews.Alert({
// // 				  model: new App.Models.MessageModel({body:'No history found for this file/folder.'}),
// // 				  type: 'info'
// // 			}));
// //     	 }
// //     },
//     serialize: function() {
//     	return { path: this.path };
//     },
//     beforeRender: function() {
//       if (this.collection.size() > 0) {
//         this.collection.each(function(historyItem) {
//           this.insertView(new AgaveIO.HistoryItem({model: historyItem, path: this.collection.path, system: this.collection.system}));
//         }, this);
//       } else if (this.__manager__.hasRendered) {
//         this.insertView('.io-history', new App.Views.Util.Alert({
//           model: new App.Models.MessageModel({'body':'There is no current history for this file/folder.'}),
//           type:'info'
//         }));
//       }
//     }
//   });
//
//   AgaveIO.HistoryList = Backbone.View.extend({
//     initialize: function() {
//       this.collection.on('add', this.render, this);
//       this.collection.on('remove', this.render, this);
//     },
//     beforeRender: function() {
//       this.collection.each(function(historyItem) {
//       	this.insertView(new AgaveIO.HistoryItem({model:historyItem}));
//       }, this);
//     }
//   });
//
//   AgaveIO.HistoryItem = Backbone.View.extend({
//   	tagName: 'li',
//     template: 'files/historyItem',
//     attributes: function() {
//       var that = this;
//       return {
//         'class': function() {
//           var classes = [
//             'agave-history',
//             'agave-history-' + that.model.get('status').toLowerCase()
//           ];
//           return classes.join(' ');
//         }
//       };
//     },
//     serialize: function() {
//       return this.model.toJSON();
//     }
//   });

  AgaveIO.Browser = Backbone.View.extend({
    template: 'files/browser',
    initialize: function() {
      this.system = this.options.collection.system;
      this.path = this.options.collection.path;
      var _this = this;

      this.collection.bind('error', function(model, error) {
      	_this.setView('.io-files', new App.Views.Util.Alert({
			    model: new App.Models.MessageModel({body:'Failed to load files. '}),
			    type: 'error'
		    }));
      }());

      if (App.Agave.token().isValid()) {

        this.collection.fetch({
        	reset:true,
          error: function(model, xhr, options) {
            console.log(xhr.responseJSON.message);
            _this.setView('.io-files', new App.Views.Util.Alert({
              model: new App.Models.MessageModel({body: xhr.responseJSON.message}),
              type: 'error'
            }));
            _this.render();
            return false;
          }
        });
        this.setView('.io-files', new UtilViews.Alert({
          model: new App.Models.MessageModel({body:'Loading your files...'}),
          type: 'info'
        }));
        this.collection.on('reset', function() {
          this.setView('.io-files', new AgaveIO.FileList({collection: this.collection}));

          var perms = new Backbone.Agave.IO.FilePermissions({path: this.collection.at(0).id, system: this.system});
          perms.on('change', function() {
            var view = new AgaveIO.ListActions({
      				model: perms,
      				file: new Backbone.Agave.IO.File({
      					path: perms.path,
      					owner: App.Agave.token().get('username'),
      					name: perms.path.substring(perms.path.lastIndexOf('/')+1),
      					system: _this.system,
      					_links: { system: { href: _this.collection.at(0).attributes._links.system.href }}
      				})
      			});
            this.setView('.actions', view);
            view.render();
          }, this);
          perms.fetch();

          this.render();
        }, this);
      } else {
        this.setView('.io-files', new UtilViews.Alert({
          model: new App.Models.MessageModel({body:'You must be logged in to view your Data.'}),
          type: 'error'
        }));
      }
    },
    serialize: function() {
      if (this.collection.size() > 0) {
      	var pparts = this.path.split('/');
//        var path = this.collection.at(0).parentDirectoryPath();
  //      var parts = path.indexOf('/') ? path.split('/') : [path];
        var pathParts = [];

        var url = '';
        for (var i = 0; i < pparts.length; i++) {
          url += '/' + pparts[i];
          pathParts.push({
            url: url,
            name: pparts[i],
            system: this.collection.system
          });
        }

        return { path: { parts: pathParts  } };
      } else {
        return { path: '' };
      }
    }
  });

  AgaveIO.FileList = Backbone.View.extend({
    initialize: function() {
      this.collection.on('add', this.render, this);
      this.collection.on('remove', this.render, this);
    },
    beforeRender: function() {
      this.collection.each(function(listing) {
      	if (listing.get('name') !== '.' && listing.get('name') !== '..') {
	    	this.insertView(new AgaveIO.File({model:listing}));
	    }
      }, this);
    }
  });

  AgaveIO.ListActions = Backbone.View.extend({
    tagName: 'span',
    initialize: function() {
      var type = this.options.file.get('type'),
        perms = this.model.get('permissions'),
        perm = _.findWhere(perms, {'username':App.Agave.token().get('username')});
      	if (perm == null) {
      		perm = _.findWhere(perms, {'username':'you'});
      	}

      if (perm.permission.read && type === 'file') {
        this.insertView(new AgaveIO.IOAction({
          model: this.options.file,
          'action': 'download',
          'label': 'Download file',
          'tagName': 'span',
          'button': true
        }));
      }
      if (perm.permission.write && type === 'dir') {
        this.insertView(new AgaveIO.IOAction({
          model: this.options.file,
          'action': 'upload',
          'label': 'Upload file',
          'tagName': 'span',
          'button': true
        }));
        this.insertView(new AgaveIO.IOAction({
          model: this.options.file,
          'action': 'create-dir',
          'label': 'Create directory',
          'tagName': 'span',
          'button': true
        }));
      }

      this.insertView(new AgaveIO.IOAction({
          model: this.options.file,
          'action': 'time',
          'link': '#files/history/' + this.model.system + '/' + this.model.id,
          'label': 'View history',
          'tagName': 'span',
          'button': true
      }));

      this.insertView(new AgaveIO.IOAction({
          model: this.options.file,
          'action': 'copy',
          'label': 'Copy file/folder',
          'tagName': 'span',
          'button': true
      }));
      this.insertView(new AgaveIO.IOAction({
          model: this.options.file,
          'action': 'rename',
          'label': 'Rename file/folder',
          'tagName': 'span',
          'button': true
      }));
      this.insertView(new AgaveIO.IOAction({
          model: this.options.file,
          'action': 'move',
          'label': 'Move file/folder',
          'tagName': 'span',
          'button': true
      }));
      this.insertView(new AgaveIO.IOAction({
          model: this.options.file,
          'action': 'transfer',
          'label': 'Transfer file/folder',
          'tagName': 'span',
          'button': true
      }));

    }
  });

  AgaveIO.IOAction = Backbone.View.extend({
    template: 'files/action',
    events: {
      'click .io-action':'doAction'
    },
    serialize: function() {
      return {
        'action':this.options.action,
        'label':this.options.label,
        'button':this.options.button? 'btn' : false,
        'link':this.options.link? this.options.link : '#'
      };
    },
    doAction: function(e) {
      e.preventDefault();
      switch (this.options.action) {
      case 'create-dir':
        var dirName = window.prompt('Please provide a name for the new directory.');
        if (dirName) {
          var that = this;
          var dir = new Backbone.Agave.IO.File({
            path: that.model.parentDirectoryPath() + "/" + dirName,
            owner: App.Agave.token().get('username'),
            name: dirName,
            type: 'dir',
            _links: { system: { href: that.model.attributes._links.system.href }}
          });
          dir.save({}, {
            success: function() {
              dir.fetch();
              that.model.collection.add(dir);
            },
            url: this.model.modelUrl(),
            type: 'PUT',
            emulateJSON: true,
            data: {
              path: dirName,
              action: 'mkdir'
            }
          });
        }
        break;

      case 'download':
        var file = this.model;
        var xhr = new XMLHttpRequest();
        xhr.open('get', this.model.downloadUrl());
        xhr.responseType = 'blob';
        // xhr.setRequestHeader(App.Agave.authHeader(), App.Agave.authValue(App.Agave.token().get('username')));
        if (App.Agave.isDevelMode()) {
        	console.log(App.Agave.jwtHeader());
        	console.log(App.Agave.jwtValue(App.Agave.token().get('username')));

        } else {
        	xhr.setRequestHeader('Authorization', 'Bearer ' + App.Agave.token().get('access_token'));
        }
        xhr.onload = function() {
          if (this.status === 200) {
            window.saveAs(new Blob([this.response]), file.get('name'));
          } else {
            var view = new UtilViews.ModalMessage({model: new App.Models.MessageModel({
              'header': 'Download failed',
              'body': this.statusText
            })});
            view.$el.on('hidden', function() {
              view.remove();
              view = null;
            });
            view.render();
          }
        };
        xhr.send();
        break;

      case 'upload':
        var view = new UtilViews.ModalView({model: new App.Models.MessageModel({header:'Upload file'})}),
          form = new AgaveIO.UploadForm({model: this.model});
        form.cleanup = function() {
          view.close();
        };
        view.insertView('.child-view', form);
        view.$el.on('hidden', function() {
          view.remove();
          view = null;
        });
        view.render();
        break;

      case 'delete':
        var message = 'Are you sure you want to delete the ' + this.model.get('type') +' "' + this.model.get('name') + '"? This operation cannot be undone.';
        if (this.model.get('type') === 'dir') {
          message += ' Any files in this dir will also be deleted!';
        }
        if (window.confirm(message)) {
          this.model.collection.remove(this.model);
          this.model.destroy({url:this.model.modelUrl()});
        }
        break;

      case 'copy':
        var newName = window.prompt('Please provide a name for the new file/folder.');
        if (newName) {
          var that = this;
          var copyItem = new Backbone.Agave.IO.File({
            path: that.model.parentDirectoryPath() + newName,
            owner: App.Agave.token().get('username'),
            name: newName.substring(newName.lastIndexOf("/")),
            type: that.model.type,
            _links: { system: { href: that.model.attributes._links.system.href }}
          });
          copyItem.save({}, {
            success: function() {
              copyItem.fetch();
              that.model.collection.add(copyItem);
            },
            url: this.model.modelUrl(),
            type: 'PUT',
            emulateJSON: true,
            data: {
              path: that.model.directoryPath() + newName,
              action: 'copy'
            }
          });
        }
        break;

      case 'move':
        var newPath = window.prompt('Please provide a new path for the new file/folder.');
        if (newPath) {
          var that = this;
          var fileItem = new Backbone.Agave.IO.File({
            path: newPath,
            owner: App.Agave.token().get('username'),
            name: newPath.substring(newPath.lastIndexOf("/")),
            type: that.model.type,
            _links: { system: { href: that.model.attributes._links.system.href }}
          });
          fileItem.save({}, {
            success: function() {
              fileItem.fetch();

              that.model.collection.add(fileItem);
            },
            url: this.model.modelUrl(),
            type: 'PUT',
            emulateJSON: true,
            data: {
              path: newPath,
              action: 'move'
            }
          });
        }
        break;

      case 'rename':
        var newName = window.prompt('Please provide a new name for the file/folder.');
        if (newName) {
          var that = this;
          var fileItem = new Backbone.Agave.IO.File({
            path: that.model.parentDirectoryPath() + "/" + newName,
            owner: App.Agave.token().get('username'),
            name: newName,
            type: that.model.type,
            _links: { system: { href: that.model.attributes._links.system.href }}
          });
          fileItem.save({}, {
            success: function() {
              fileItem.fetch();
              that.model.collection.add(fileItem);
            },
            url: this.model.modelUrl(),
            type: 'PUT',
            emulateJSON: true,
            data: {
              path: that.model.parentDirectoryPath() + "/" + newName,
              action: 'rename'
            }
          });
        }
        break;

      // case 'time':
// 		new AgaveIO.History({
// 			model: this.model,
// 			el: this.$el.find('.file-history')
// 		}).render();
// 		$(e.target).hide();
// 		break;
//
      }
      return false;
    }
  });

  AgaveIO.UploadForm = Backbone.View.extend({
    template: 'files/upload',
    tagName: 'form',
    events: {
      'click .btn-upload': 'doUpload',
      'click .btn-cancel': 'cancelUpload'
    },
    doUpload: function(e) {
      e.preventDefault();
      if (this.el.fileToUpload.files.length > 0) {
        var formData = new FormData();
        var fileToUpload = this.el.fileToUpload.files[0];
        formData.append('fileToUpload', fileToUpload);
        var xhr = new XMLHttpRequest();
        var url = this.model.downloadUrl();
        xhr.open('POST', url , true);
        xhr.setRequestHeader('Authorization', 'Bearer ' + App.Agave.token().get('access_token'));
        var that = this;
        xhr.onload = function() {
          if (this.status === 202) {
            that.remove();
            var responseJson = JSON.parse(this.response);
            // bug
            if (responseJson.result.path.indexOf('/') !== 0) {
              responseJson.result.path = '/' + responseJson.result.path;
            }
            var file = new Backbone.Agave.IO.File(responseJson.result);
            //that.model.collection.add(file);
          }
          else
          {
            this.remove();
            var view = new UtilViews.ModalMessage({model: new App.Models.MessageModel({
              'header': 'Upload failed',
              'body': this.statusText
            })});
            view.$el.on('hidden', function() {
              view.remove();
              view = null;
            });
            view.render();
          }
        };
        xhr.send(formData);
      }
      return false;
    },
    cancelUpload: function(e) {
      e.preventDefault();
      this.remove();
      return false;
    }
  });

  AgaveIO.File = Backbone.View.extend({
    template: 'files/listing',
    attributes: function() {
      var that = this;
      return {
        'class': function() {
          var classes = [
            'io-listing',
            'io-' + that.model.get('type'),
            'io-' + that.model.get('format')
          ];
          if (that.model.get('name') === '..') {
            classes.push('io-previous');
          }
          return classes.join(' ');
        }
      };
    },
    events: {
      'click .io-actions': 'showActions'
    },
    showActions: function() {
      if (! this.permissions) {
        this.permissions = new Backbone.Agave.IO.FilePermissions({'path':this.model.id, system: this.system()});
        this.permissions.on('change', function() {
          var perms = _.findWhere(this.permissions.get('permissions'), {'username':'you'}),
            type = this.model.get('type');

          if (perms == null) {
          	perms = _.findWhere(this.permissions.get('permissions'), {'username':App.Agave.token().get('username')});
          }

          this.$el.find('.dropdown-menu').empty();

          this.insertView('.dropdown-menu', new AgaveIO.IOAction({model:this.model, action:'time',label:'History',tagName:'li', 'link': '#files/history/' + this.model.system + '/' + this.model.id}));

          if (this.permissions.attributes["0"].permission.read) {
            if (type === 'file') {
              this.insertView('.dropdown-menu', new AgaveIO.IOAction({model:this.model, action:'download',label:'Download file',tagName:'li'}));
            }
          }

          if (this.permissions.attributes["0"].permission.write) {
            if (type === 'dir') {
              // new directory
              this.insertView('.dropdown-menu', new AgaveIO.IOAction({model:this.model, action:'create-dir',label:'Create directory',tagName:'li'}));
              this.insertView('.dropdown-menu', new AgaveIO.IOAction({model:this.model, action:'upload',label:'Upload',tagName:'li'}));
            }
            // rename
            this.insertView('.dropdown-menu', new AgaveIO.IOAction({model:this.model, action:'rename',label:'Rename',tagName:'li'}));
            // // move
            this.insertView('.dropdown-menu', new AgaveIO.IOAction({model:this.model, action:'move',label:'Move',tagName:'li'}));
            // // copy
            this.insertView('.dropdown-menu', new AgaveIO.IOAction({model:this.model, action:'copy',label:'Copy',tagName:'li'}));
            // delete
            this.insertView('.dropdown-menu', new AgaveIO.IOAction({model:this.model, action:'delete',label:'Delete',tagName:'li'}));
          }

          if (this.permissions.attributes["0"].username === 'you' || this.permissions.attributes["0"].username === App.Agave.token().get('username')) {
            // share
            this.insertView('.dropdown-menu', new AgaveIO.IOAction({model:this.model, action:'share',label:'Share',tagName:'li'}));
          }
          this.getViews('.dropdown-menu').each(function(view) { view.render(); });
        }, this);
        this.permissions.fetch();
      }
    },
    system: function() {
    	var sys = this.model.attributes._links.system.href;
    	return sys.substring(sys.lastIndexOf("/") + 1);
    },
    serialize: function() {
      var model = this.model, json = model.toJSON();
      //json.lastModified = moment(json.lastModified).format('MMM DD, YYYY);
      json.system = this.system();
      return json;
    }
  });

  AgaveIO.FileChooser = App.Views.FormViews.Field.extend({
    template: 'files/filechooser',
    events: {
      'click .btn-choose-file': 'chooseFile'
    },
    initialize: function() {
      this.listenTo(this.model, 'change', this.render);
    },
    chooseFile: function(e) {
      e.preventDefault();
      var view = this, chooser = new UtilViews.ModalView({model: new App.Models.MessageModel({
        header: 'Choose ' + this.model.get('label'),
        body: this.model.get('help')
      })});

      var dialog = new AgaveIO.FileChooserDialog();
      dialog.cleanup = function() {
        chooser.close();
        view.model.set('defaultValue', dialog.selection);
      };
      chooser.setView('.child-view', dialog);
      chooser.render();
      chooser.$el.on('hidden', function() {
        chooser.remove();
      });
      return false;
    }
  });

  AgaveIO.FileChooserDialog = Backbone.View.extend({
    initialize: function() {
      this.system = 'data.iplantcollaborative.org';
      this.collection = new Backbone.Agave.IO.Listing([], {path: App.Agave.token().get('username'), system: this.system});
      this.listenTo(this.collection, 'reset', this.render);
      this.collection.fetch({reset:true});
    },
    beforeRender: function() {
      if (this.collection.size() === 0 && ! this.__manager__.hasRendered) {
        this.insertView(new UtilViews.Alert({model: new App.Models.MessageModel({body: 'Loading your files...'})}));
      } else {
        this.template = 'files/filechooser-dialog';
        this.collection.each(function(item) {
          this.insertView('.io-chooser', new AgaveIO.FileChooserItem({model: item}));
        }, this);
        this.insertView('.io-chooser-actions', new AgaveIO.FileChooserActions());
      }
    },
    events: {
      'click .io-chooser-item': function(e) {
        var item = $(e.currentTarget);
        this.currentModel = this.collection.at(item.index());
        item.addClass('active').siblings().removeClass('active');
        var actions = this.getView('.io-chooser-actions');
        actions.model = this.currentModel;
        actions.render();
      },
      'click .btn-browse': function() {
        var path = this.currentModel.get('path');
        if (this.currentModel.get('name') === '..') {
          path = path.slice(0, path.lastIndexOf('/'));
        }
        this.collection.path = path.slice(1);
        this.collection.fetch({reset: true});
      },
      'click .btn-choose': function() {
        this.selection = this.currentModel.get('path');
        this.remove();
      },
      'click .btn-cancel': function() {
        this.remove();
      }
    }
  });

  AgaveIO.FileChooserActions = Backbone.View.extend({
    template: 'files/filechooser-actions',
    serialize: function() {
      var json = {model: typeof this.model !== 'undefined'};
      if (json.model) {
        json.directory = this.model.get('type') === 'dir';
      }
      return json;
    }
  });

  AgaveIO.FileChooserItem = Backbone.View.extend({
    template: 'files/filechooser-item',
    className: 'io-chooser-item',
    serialize: function() {
    	if (this.model.attributes.name != '.') {
      		return this.model.toJSON();
      	} else {
      		return false;
      	}
    }
  });

  App.Views.AgaveIO = AgaveIO;
  return AgaveIO;
});
