/*global define*/
'use strict';
define(['app'], function(App) {

  var AgaveJobs = {};


  AgaveJobs.List = Backbone.View.extend({
    tagName: 'div',
    template: 'jobs/list',
    initialize: function() {
      this.collection = new Backbone.Agave.Jobs.Jobs();
      this.listenTo(this.collection, 'reset', this.render);
      this.collection.fetch({reset:true});
    },
    beforeRender: function() {
      if (this.collection.size() > 0) {
        this.collection.each(function(job) {
          this.insertView('.results', new AgaveJobs.ListItem({
            className: 'search-result',
            model: job}));
        }, this);
      } else if (this.__manager__.hasRendered) {
        this.insertView(new App.Views.Util.Alert({
          model: new App.Models.MessageModel({'body':'You currently don\'t have any jobs.'}),
          type:'info'
        }));
      }
    }
  });

  AgaveJobs.ListItem = Backbone.View.extend({
    template: 'jobs/job',
    serialize: function() {
      if (!this.model.get('endTime')) {
        this.model.set('endTime', '--');
      }
      if (!this.model.get('startTime')) {
        this.model.set('startTime', '--');
      }
      console.log(this.model.toJSON());
      return this.model.toJSON();
    }
  });

  AgaveJobs.History = Backbone.View.extend({
    tagName: 'ul',
    template: 'jobs/history',
    initialize: function() {
      this.collection = this.model.history;
      this.listenTo(this.collection , 'reset', this.render);
      this.collection.fetch({reset:true});
    },
    beforeRender: function() {
      if (this.collection.size() > 0) {
        this.collection.each(function(historyItem) {
          this.insertView(new AgaveJobs.HistoryItem({model: historyItem}));
        }, this);
      } else if (this.__manager__.hasRendered) {
        this.insertView(new App.Views.Util.Alert({
          model: new App.Models.MessageModel({'body':'You currently don\'t have any history entires for this job'}),
          type:'info'
        }));
      }
    }
    // serialize: function() {
    //   return { jobId: this.model.get("id") };
    // }
  });

  AgaveJobs.HistoryItem = Backbone.View.extend({
    tagName: 'li',
    template: 'jobs/historyItem',
    serialize: function() {
      console.log(this.model.toJSON());
      return this.model.toJSON();
    }
  });

  AgaveJobs.JobView = Backbone.View.extend({
    template: 'jobs/view',
    initialize: function() {
      if (! this.model.collection) {
        this.model.on('change', function() {

          this.$el.find('.dropdown-menu').empty();

          this.insertView('.dropdown-menu', new AgaveJobs.JobAction({
            model: this.model,
            'action': 'time',
            'link': '#jobs/' + this.model.id + '/history',
            'label': 'View history',
            'tagName': 'li'
          }));

          this.insertView('.dropdown-menu', new AgaveJobs.JobAction({
            model: this.model,
            'action': 'browse',
            'link': '#jobs/' + this.model.id + '/output',
            'label': 'View output',
            'tagName': 'li'
          }));

          this.insertView('.dropdown-menu', new AgaveJobs.JobAction({
            model: this.model,
            'action': 'annotate',
            'link': '#jobs/' + this.model.id + '/metadata',
            'label': 'View metadata',
            'tagName': 'li'
          }));

          this.insertView('.dropdown-menu', new AgaveJobs.JobAction({
            model: this.model,
            'action': 'notifications',
            'link': '#jobs/' + this.model.id + '/notifications',
            'label': 'View notifications',
            'tagName': 'li'
          }));

          this.insertView('.dropdown-menu', new AgaveJobs.JobAction({
            model: this.model,
            'action': 'resubmit',
            'label': 'Resubmit',
            'tagName': 'li'
          }));


          this.insertView('.dropdown-menu', new AgaveJobs.JobAction({
            model: this.model,
            'action': 'share',
            'label': 'Share',
            'tagName': 'li'
          }));


          this.insertView('.dropdown-menu', new AgaveJobs.JobAction({
            model: this.model,
            'action': 'delete',
            'label': 'Delete',
            'tagName': 'li'
          }));

          this.insertView('.dropdown-menu', new AgaveJobs.JobAction({
            model: this.model,
            'action': 'stop',
            'label': 'Stop',
            'tagName': 'li'
          }));

          this.getViews('.dropdown-menu').each(function(view) { view.render(); });
          this.render();
        }, this);
        this.model.fetch();
      }
    },
    serialize: function() {
      var json = this.model.toJSON();
      if (this.model.collection) {
        json.hasCollection = true;
      }
      if (!json.endTime) {
        json.endTime = '--';
      }
      if (!json.startTime) {
        json.startTime =   '--';
      }

      json.archive = json.archive ? 'true' : 'false';

      if (!json.localId) {
        json.localId = '--';
      }

      for (var key in json.inputs) {
        if (!(json.inputs[key].indexOf('http://') == 0 ||
            json.inputs[key].indexOf('https://') == 0 ||
            json.inputs[key].indexOf('agave://') == 0)) {
          json.inputs[key] = { name: json.inputs[key], value: "default/" + json.inputs[key] };
        } else {
          json.inputs[key] = { name: json.inputs[key], value: json.inputs[key]};
        }
      }

      console.log(json);
      return json;
    },
    events: {
      'click .back-to-list': 'backToCollection',
      'click .btn-show-permissions': 'showPermissions'
    },
    backToCollection: function(e) {
      e.preventDefault();
      this.remove();
      App.router.navigate('#jobs');
      $('html,body').animate({scrollTop:view.$el.position().top - 100});
    },
    showPermissions: function(e) {
      e.preventDefault();
      if (App.Agave.token().isValid()) {
        new AgaveMetadata.MetadataPermissions({
          collection: new Backbone.Agave.Jobs.JobPermissionList({ id: this.model.id }),
          el: this.$el.find('.app-form')
        }).render();
        $(e.target).hide();
        this.$el.find('.btn-show-form').show();
      } else {
        alert('You must be logged in to manage metadata permissions.');
      }
    },
    showActions: function(e) {
      console.log(e);    }
  });

  AgaveJobs.ListActions = Backbone.View.extend({
    tagName: 'span',
    initialize: function() {

      this.insertView(new AgaveJobs.JobAction({
        model: this.options.job,
        'action': 'time',
        'link': '#jobs/' + this.options.job.id + '/history',
        'label': 'View history',
        'tagName': 'li'
      }));

      this.insertView(new AgaveJobs.JobAction({
        model: this.options.job,
        'action': 'browse',
        'link': '#jobs/' + this.options.job.id + '/output',
        'label': 'View output',
        'tagName': 'li'
      }));

      this.insertView(new AgaveJobs.JobAction({
        model: this.options.job,
        'action': 'annotate',
        'link': '#jobs/' + this.options.job.id + '/metadata',
        'label': 'View metadata',
        'tagName': 'li'
      }));

      this.insertView(new AgaveJobs.JobAction({
        model: this.options.job,
        'action': 'notifications',
        'link': '#jobs/' + this.options.job.id + '/notifications',
        'label': 'View notifications',
        'tagName': 'li'
      }));

      this.insertView(new AgaveJobs.JobAction({
        model: this.options.job,
        'action': 'resubmit',
        'label': 'Resubmit',
        'tagName': 'li'
      }));


      this.insertView(new AgaveJobs.JobAction({
        model: this.options.job,
        'action': 'share',
        'label': 'Share',
        'tagName': 'li'
      }));


      this.insertView(new AgaveJobs.JobAction({
        model: this.options.job,
        'action': 'delete',
        'label': 'Delete',
        'tagName': 'li'
      }));

      this.insertView(new AgaveJobs.JobAction({
        model: this.options.job,
        'action': 'stop',
        'label': 'Stop',
        'tagName': 'li'
      }));

    }
  });

  AgaveJobs.JobAction = Backbone.View.extend({
    template: 'jobs/action',
    events: {
      'click .io-action':'doAction'
    },
    serialize: function() {
      return {
        'action':this.options.action,
        'label':this.options.label,
        'button':this.options.button? 'btn' : false,
        'link':this.options.link? this.options.link : '#'
      };
    },
    doAction: function(e) {
      e.preventDefault();
      switch (this.options.action) {
      case 'time':
        break;
      case 'browse':
        break;
      case 'annotate':
        break;
      case 'notifications':
        break;
      case 'resubmit':
        var that = this;
        var job = new Backbone.Agave.Jobs.Job({
          id: that.model.id
        });
        job.save({}, {
          success: function() {
            job.fetch();
            if (job.id != that.model.id) {
              window.alert('Your job has been resubmitted successfully as job ' + job.id);
            } else {
              window.alert('There was an error resubmitting your job');
            }
          },
          url: this.model.modelUrl(),
          type: 'PUT',
          emulateJSON: true,
          data: {
            action: 'resubmit'
          }
        });
        break;
      case 'share':
        break;
      case 'delete':
        this.model.destroy({}, {
          success: function() {
            App.router.navigate('#jobs', {
              trigger: true
            });
          },
          error: function() {
            window.alert('Failed to delete job.');
          }
        })
        break;
      case 'stop':
        break;
      }
      return false;
    }
  });

  App.Views.AgaveJobs = AgaveJobs;

  return AgaveJobs;
});
