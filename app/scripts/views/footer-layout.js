/*global define, Backbone*/
'use strict';
define(['app'], function(App){
  var FooterLayoutView = Backbone.View.extend();

  App.Views.FooterLayoutView = FooterLayoutView;
  return FooterLayoutView;
});
