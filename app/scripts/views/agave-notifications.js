/*global define, Backbone, _*/
'use strict';
define(['app'], function(App) {
  var AgaveNotification = {};

  AgaveNotification.List = Backbone.View.extend({
    tagName: 'div',
    template: 'notifications/list',
    initialize: function() {
      this.collection = new Backbone.Agave.Notification.Notifications();
      this.listenTo(this.collection, 'reset', this.render);
      this.collection.fetch({reset:true});
    },
    beforeRender: function() {
      if (this.collection.size() > 0) {
        this.collection.each(function(notification) {
          for (var key in notification.get('_links')) {
            if (key != 'self') {
              notification.set('type',key.toUpperCase());
              notification.set('associated_resource', notification.get('_links')[key].href);
              this.insertView('.results', new AgaveNotification.ListItem({
                className: 'search-result',
                model: notification}));
            }
          }
        }, this);
      } else if (this.__manager__.hasRendered) {
        this.insertView(new App.Views.Util.Alert({
          model: new App.Models.MessageModel({'body':'You currently don\'t have any Notification URLs.'}),
          type:'info'
        }));
      }
    }
  });

  AgaveNotification.ListItem = Backbone.View.extend({
    template: 'notifications/view',
    serialize: function() {
      console.log(this.model.toJSON());
      return this.model.toJSON();
    }
  });

  AgaveNotification.CreateForm = Backbone.View.extend({
    tagName:'form',
    template: 'notifications/form',
    initialize: function() {
      this.model = new Backbone.Agave.Notification.Notification();
    },
    beforeRender: function() {
      var FormViews = App.Views.FormViews;

      this.insertViews({'.form-fields': [
        new FormViews.Text({
          model: new App.Models.FormModel({
            label: 'Callback URL or Email',
            help: 'This is the URL or email address that will be invoked by this Notification',
            name: 'url',
            id: 'url',
            defaultValue: this.model.get('url'),
            required: true
          })
        }),
        new FormViews.Text({
          model: new App.Models.FormModel({
            label: 'Target Event',
            help: 'This is the Agave Evetn with which will trigger the notification, ex. FINISHED. Enter * for all events.',
            name: 'event',
            id: 'event',
            defaultValue: this.model.get('event'),
            required: true
          })
        }),
        new FormViews.Text({
          model: new App.Models.FormModel({
            label: 'Resource UUID',
            help: 'UUID of the resource which will generate the event.',
            name: 'associatedUuid',
            id: 'associatedUuid',
            defaultValue: this.model.get('associatedUuid'),
            required: true
          })
        }),
        new FormViews.Checkbox({
          model: new App.Models.FormModel({
            label: 'Make Persistent',
            help: 'Should this notification be saved and reused after the first event occurs?',
            name: 'persistent',
            id: 'persistent',
            defaultValue: this.model.get('persistent') || false
          })
        })
      ]});

      this.insertViews({'.form-actions': [
        new FormViews.Button({
          'name': 'notification-create-submit',
          'id': 'notification-create-submit',
          'type': 'submit',
          'class': 'btn btn-success',
          'value': 'Create Notification'
        }),
        new FormViews.Button({
          'name': 'notification-create-cancel',
          'id': 'notification-create-cancel',
          'value': 'Cancel'
        })
      ]});
    },
    events: {
      'click #notification-create-submit': 'submitForm',
      'click #notification-create-cancel': 'cancelForm'
    },
    submitForm: function(e) {
      e.preventDefault();
      var formValues = this.$el.serializeArray();
      var post = {};
      var password;

      _.each(formValues, function(value) {
        if (value.name === 'password') {
          password = value.value;
        } else {
          post[value.name] = value.value;
        }
      });
      this.model.save({}, {
        emulateJSON: true,
        data: post,
        password: password,
        success: function() {
          Backbone.history.navigate('#notification', {'trigger':true});
        }
      });
      return false;
    },
    cancelForm: function(e) {
      e.preventDefault();
      var fragment;
      if (App.Agave.token().isValid()) {
        fragment = '#notification';
      } else {
        fragment = '#';
      }
      Backbone.history.navigate(fragment, {'trigger':true});
      return false;
    }
  });

  AgaveNotification.Search = Backbone.View.extend({
    template: 'notifications/search',
    initialize: function() {
      this.listenTo(this.collection, 'reset', this.render);
    },
    beforeRender: function() {
      if (! App.Agave.token().isValid()) {
        this.setView('.search', new App.Views.Util.Alert({
          model: new App.Models.MessageModel({
            body: 'You must be logged in to search notifications.'
          }),
          type: 'error'
        }));
      } else if (this.collection.length > 0) {
        this.collection.each(function(notification) {
          for (var key in notification.get('_links')) {
            if (key != 'self') {
              notification.set('type',key.toUpperCase());
              notification.set('associated_resource', notification.get('_links')[key].href);
              this.insertView('.results', new AgaveNotification.ListItem({
                className: 'search-result',
                model: notification}));
            }
          }
        }, this);
      } else if (this.__manager__.hasRendered) {
        this.insertView('.search', new App.Views.Util.Alert({
          model: new App.Models.MessageModel({body: 'No results for search.'}),
          type: 'info'
        }));
      }
    },
    serialize: function() {
      return {
        keyword: this.collection.keyword,
        results: this.collection.length > 0
      };
    },
    events: {
      'click .btn-search': 'doSearch'
    },
    doSearch: function(e) {
      e.preventDefault();
      var view = this;

      // var formValues = this.$el.find('form[name=notification-search-form]').serializeArray();
      //
      // _.each(formValues, function(val) {
      //   this.collection[val.name] = $('#keyword').val();
      // }, this);

      this.collection["associatedUuid"] = $('#keyword').val();

      this.collection.fetch({
        reset: true,
        beforeSend: function() {
          var loading = new App.Views.Util.Loading();
          view.insertView('.search', loading);
          loading.render();
        },
        error: function(collection, response) {
          var resp = $.parseJSON(response.responseText);
          window.alert(resp.message);
        }
      });

      return false;
    }
  });

  App.Views.AgaveNotification = AgaveNotification;
  return AgaveNotification;
});
