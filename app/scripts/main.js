/*global define, require*/
'use strict';
require.config({
  paths: {
    'jquery': '../components/jquery/jquery',
    'underscore': '../components/underscore/underscore',
    'backbone': '../components/backbone/backbone',
    'layoutmanager': '../components/layoutmanager/backbone.layoutmanager',
    'bootstrap': '../components/bootstrap/docs/assets/js/bootstrap',
    'moment': '../components/moment/moment',
    'humanize': '../components/humanize/humanize',
    'handlebars': '../components/handlebars/handlebars',
    'backbone-agave': '../components/backbone-agave/backbone-agave',
    'backbone-agave-postits': '../components/backbone-agave/backbone-agave-postits',
    'backbone-agave-notifications': '../components/backbone-agave/backbone-agave-notifications',
    'backbone-agave-metadata': '../components/backbone-agave/backbone-agave-metadata',
//     'backbone-agave-schemata': '../components/backbone-agave/backbone-agave-schemata',
    'backbone-agave-profiles': '../components/backbone-agave/backbone-agave-profiles',
    'backbone-agave-systems': '../components/backbone-agave/backbone-agave-systems',
    'backbone-agave-files': '../components/backbone-agave/backbone-agave-files',
//     'backbone-agave-transforms': '../components/backbone-agave/backbone-agave-transforms',
    'backbone-agave-jobs': '../components/backbone-agave/backbone-agave-jobs',
    'backbone-agave-apps': '../components/backbone-agave/backbone-agave-apps',
    'file-saver': '../components/file-saver/FileSaver',
    'oauthd': '../components/backbone-agave/oauthd'
  },
  shim: {
    handlebars: {
      exports: 'Handlebars'
    },
    underscore: {
      exports: '_'
    },
    backbone: {
      deps: ['jquery', 'underscore'],
      exports: 'Backbone'
    },
    layoutmanager: {
      deps: ['backbone'],
      exports: 'layoutmanager'
    },
    bootstrap: {
      deps: ['jquery'],
      exports: 'jquery'
    },
    oauthd: {
      deps: ['jquery'],
      exports: 'OAuth'
    },
    'backbone-agave': {
      deps: ['backbone'],
      exports: 'Backbone.Agave'
    },
    'backbone-agave-postits': {
      deps: ['backbone', 'backbone-agave'],
      exports: 'Backbone.Agave.PostIts'
    },
    'backbone-agave-notifications': {
      deps: ['backbone', 'backbone-agave'],
      exports: 'Backbone.Agave.Notification'
    },
    'backbone-agave-metadata': {
      deps: ['backbone', 'backbone-agave'],
      exports: 'Backbone.Agave.Metadata'
    },
//     'backbone-agave-schemata': {
//       deps: ['backbone', 'backbone-agave', 'backbone-agave-metadata'],
//       exports: 'Backbone.Agave.Schemata'
//     },
    'backbone-agave-profiles': {
      deps: ['backbone', 'backbone-agave'],
      exports: 'Backbone.Agave.Profiles'
    },
    'backbone-agave-systems': {
      deps: ['backbone', 'backbone-agave'],
      exports: 'Backbone.Agave.Systems'
    },
    'backbone-agave-files': {
      deps: ['backbone', 'backbone-agave'],
      exports: 'Backbone.Agave.Files'
    },
//     'backbone-agave-transforms': {
//       deps: ['backbone', 'backbone-agave', 'backbone-agave-systems', 'backbone-agave-files'],
//       exports: 'Backbone.Agave.Transforms'
//     },
    'backbone-agave-jobs': {
      deps: ['backbone', 'backbone-agave', 'backbone-agave-systems', 'backbone-agave-files'],
      exports: 'Backbone.Agave.Jobs'
    },
    'backbone-agave-apps': {
      deps: ['backbone', 'backbone-agave'],
      exports: 'Backbone.Agave.Apps'
    }

  }
});

define([
  'app',
  'jquery',
  'oauthd',
  'bootstrap',
  'moment',
  'handlebars',
  'humanize',
  'file-saver',
  'backbone-agave',
  'backbone-agave-postits',
  'backbone-agave-notifications',
  'backbone-agave-metadata',
//   'backbone-agave-schemata',
  'backbone-agave-profiles',
  'backbone-agave-systems',
  'backbone-agave-files',
//   'backbone-agave-transforms',
  'backbone-agave-jobs',
  'backbone-agave-apps',
  'models/message',
  'models/form',
  'views/app-views',
  'views/form-views',
  'views/util-views',
  'views/agave-auth',
  'views/agave-postits',
  'views/agave-notifications',
  'views/agave-metadata',
//   'views/agave-schemata',
  'views/agave-profiles',
  'views/agave-systems',
  'views/agave-files',
//   'views/agave-transforms',
  'views/agave-jobs',
  'views/agave-apps',
  'routers/default'
], function(App) {
  App.start();
});
